import java.io.FileInputStream;
import java.io.InputStream;

/**
 * throws 表示方法准备要扔出来一个异常
 * 产生的错误尽可能的自己处理，少向外抛出异常
 *
 * throw表示向外抛出异常
 */
public class Test {
//    public static void read() throws Exception{
//        InputStream is = new FileInputStream(new File("哈哈哈"));
//    }
    public static void chu(int a, int b)throws Exception{
        if (b==0){
            throw new Exception("你不可以给我一个0"); // 真正向外抛出一个异常
        }else{
            System.out.println(a/b);
        }
    }

    public static void main(String[] args) {
//        read();
        try {
            chu(1, 0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
