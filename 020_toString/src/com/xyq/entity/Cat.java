package com.xyq.entity;

public class Cat {
    String name;
    String color;

    public Cat(String name, String color){
        this.name = name;
        this.color = color;
    }

    @Override
    public String toString() {
//        return "Cat{" +
//                "name='" + name + '\'' +
//                ", color='" + color + '\'' +
//                '}';
        return "我的猫"+"颜色";
    }

    public static void main(String[] args){
        Cat c = new Cat("小花", "绿色");
        // 直接打印这个C
        System.out.println(c);

        // 默认打印对象，自动的执行这个对象中的toString()方法
        System.out.println(c.toString());
        // 默认的toString-> 包+类@内存地址


    }
}
