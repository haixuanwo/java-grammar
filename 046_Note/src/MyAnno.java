/**
 *  自定义注解
 *  本质：注解本质上就是一个接口，该接口默认继承Annotation接口
 *  public interface MyAnno extends java.lang.annotation.Annotation {}
 *
 * public @interface 注解名称{
 *     属性列表；
 * }
 *
 *  属性：接口中可以定义的成员方法
 *      要求：
 *      1、属性的返回值类型有下列取值
 *          基本数据类型
 *          String
 *          枚举
 *          注解
 *          以上类型的数组
 *      2、定义了属性，在使用时需要给属性赋值
 *          （1）如果定义属性时，使用default关键字给属性默认初始化值，则使用注解时，可不进行属性赋值
 *          （2）如果只有一个属性需要赋值，并且属性的名称是value，则value可以省略，直接定义值即可
 *           (3) 数组赋值时，值使用{}包裹，只一个值{}可省略
 *
 */
public @interface MyAnno {

    int value();
//    String show2();

    Person per();
    MyAnno2 anno2();

    String[] strs();
}
