/**
 *  1、JDK中预定义的一些注解
 *  @override    检测方法是否继承父类（接口）
 *  @Deprecated  标注已过时
 *  @SuppressWarnings  压制警告
 */
@SuppressWarnings("all")    // 压制所有警告
public class AnnotationDemo1 {
    @Override
    public String toString(){
        return new String();
    }

    @Deprecated
    public void show1(){
        // 有缺陷
    }

    public void show2(){
        // 替代show1
    }

    public void demo(){
        show1();
    }
}
