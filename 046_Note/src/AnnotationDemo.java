/**
 * @author haixuanwo
 * @version 0.1
 *
 * 注解：说明程序，给计算机看
 * 注释：用文字描述程序，给程序员看
 * 定义：注解，也叫元数据。一种代码级别的说明
 *
 * 作用分类：
 * 1、编写文档：通过代码里标识的元数据生成文档
 * 2、代码分析：通过代码里标识的元数据对代码进行分析【使用反射】
 * 3、编译检查：通过代码里标识的元数据让编译器能够实现基本的编译检查【Override】
 *
 * 使用注解：@注解名称
 *
 * 3、在程序中使用（解析）注解：获取注解中定义的属性值
 *      注解大多用于替换配置文件
 *      (1)获取注解定义的位置的对象 (Class,Method,Field)
 *      (2)获取指定注解 getAnnotation(Class)
 *      (3)调用注解中的抽象方法获取配置的属性值
 *
 * 小结：
 * （1）大多数时候是使用注解，少自定义注解
 * （2）注解给谁用
 *      ① 编译器
 *      ② 解析程序用
 * （3）注解不是程序的一部分，可以理解为标签
 */
public class AnnotationDemo {
    /**
     * 计算两数的和
     * @param a 整数
     * @param b 整数
     * @return 和结果
     */
    public int add(int a, int b){
        return a + b;
    }
}
