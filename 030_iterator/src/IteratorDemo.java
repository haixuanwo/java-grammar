import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 迭代器：对集合进行遍历
 * boolean hasNext() 若仍有元素则返回true
 * E next() 返回迭代的下一个元素
 *
 * Iterator迭代器是一个接口，无法直接使用需要Iterator接口实现类对象
 * Collection接口的方法iterator()返回迭代器的实现类
 *
 * 使用步骤
 * 1、使用iterator()获取迭代器
 * 2、hasNext判断是否还有元素
 * 3、next取出下一个元素
 */
public class IteratorDemo {
    public static void main(String[] args) {
        Collection<String> coll = new ArrayList<>();
        coll.add("你大爷");
        coll.add("谭慧芳");
        coll.add("谭恒");

        Iterator<String> it = coll.iterator();
        while (it.hasNext()){
            String e = it.next();
            System.out.println(e);
        }
    }
}
