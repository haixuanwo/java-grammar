
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

/**
 * Properties类表示持久化属性集，可保存在流中或从流中加载
 *  stor 存到硬盘
 *  load 从硬盘加载
 */
public class PropertiesDemo {
    public static void main(String[] args) {
        try{
            test1();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void test1() throws IOException {
        Properties prop = new Properties();
        prop.setProperty("杨过","000");
        prop.setProperty("小龙女","001");
        prop.setProperty("郭襄","002");

        Set<String> set = prop.stringPropertyNames();
        for (String s : set) {
            String value = prop.getProperty(s);
            System.out.println(s+"="+value);
        }

        prop.store(new FileOutputStream("prop.txt"),"");

        Properties prop1 = new Properties();
        prop1.load(new FileReader("prop.txt"));
        set = prop1.stringPropertyNames();
        for (String s : set) {
            String value = prop.getProperty(s);
            System.out.println(s+"="+value);
        }
    }
}
