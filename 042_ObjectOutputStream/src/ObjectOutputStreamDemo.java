import java.io.*;

/**
 * 一、ObjectOutputStream 对象的序列化流，把对象以流的方式写入文件
 * ObjectOutputStream(OutputStream out)
 *
 * OutputStream(Object obj)
 *
 * 二、ObjectInputStream 对象反序列化
 * ObjectInputStream(InputStream in)
 *
 *
 */
public class ObjectOutputStreamDemo {
    public static void main(String[] args) {
        try{
            test1();
        }catch (IOException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static void test1() throws IOException,ClassNotFoundException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("a.txt"));
        oos.writeObject(new Person("刘亦菲", 16));
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("a.txt"));
        Object o = ois.readObject();
        ois.close();
        System.out.println(o);
        Person p = (Person)o;
        System.out.println(p.getName()+p.getAge());
    }
}
