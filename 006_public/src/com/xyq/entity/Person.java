package com.xyq.entity;

public class Person {

    /**
    * 1.public公共的，所有人都可以访问
    * 2、default包访问权限，在自己包内可以随意访问
    * 3、private私有的只有自己能访问
    */
    public String pub = "public";
    private String pri = "private";
    String def = "default";

    public static void main(String[] args){
        Person p = new Person();
        System.out.println(p.pub);
        System.out.println(p.pri);
        System.out.println(p.def);
    }
}
