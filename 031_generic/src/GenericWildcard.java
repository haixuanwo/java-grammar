import java.util.ArrayList;
import java.util.Iterator;

/**
 * 泛型通配符：
 *      ?：代表任意的数据类型
 *
 *  使用方式：
 *      不能创建对象使用，只能作为方法的参数使用
 *
 *   泛型上限定: ? extends E 代表使用的泛型只能是E类型的子类/本身
 *   泛型下限定: ? super E 代表使用的泛型只能是E类型的父类/本身
 */
public class GenericWildcard {
    public static void main(String[] args) {
        ArrayList<String> al1 = new ArrayList<>();
        al1.add("我草了");
        al1.add("有这事");
        al1.add("天哪");
        printArray(al1);

        ArrayList<Integer> al2 = new ArrayList<>();
        al2.add(100);
        al2.add(200);
        al2.add(300);
        printArray(al2);

    }

    public static void printArray(ArrayList<?> list){
        Iterator<?> it = list.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
}
