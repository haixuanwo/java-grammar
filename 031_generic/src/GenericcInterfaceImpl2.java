/**
 * 含泛型接口：类跟着接口为泛型类
 */
public class GenericcInterfaceImpl2<I> implements GenericInterface<I>{
    @Override
    public void method(I i) {
        System.out.println(i);
    }

    public static void main(String[] args) {
        GenericcInterfaceImpl2 gi2 = new GenericcInterfaceImpl2();
        gi2.method("10001");
        gi2.method("我了个草");
    }
}
