/**
 * 泛型：一种未知的数据类型，可用作变量来接收数据类型。创建集合时确定类型
 *
 * 1、类泛型
 * 2、方法泛型
 * 3、接口泛型
 * 4、通配符
 *
 */
public class CGeneric implements GenericInterface<String>{

    @Override
    public void method(String i) {
        System.out.println(i);
    }

    public static void main(String[] args) {
        MyNum<Integer> mn = new MyNum<Integer>();
        System.out.println(mn.add(2, 100));

        GenericMethod gm = new GenericMethod();
        gm.method01(100);
        gm.method01("擦你妈");

        GenericMethod.method02(100);
        GenericMethod.method02("噶你吗");

        CGeneric cg = new CGeneric();
        cg.method("我赶了你大爷");
    }
}
