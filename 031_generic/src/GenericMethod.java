/**
 * 泛型定义在方法的修饰符合返回值类型
 *  格式：
 *      修饰符<泛型> 返回值类型 方法名(参数列表){
 *          方法体；
 *      }
 */
public class GenericMethod {
    public <M> void method01(M m){
        System.out.println(m);
    }

    public static <M> void method02(M m){
        System.out.println(m);
    }
}
