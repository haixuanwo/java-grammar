/**
 * 所有的字符串字面值都作为String类的实例实现，没有new也照样是
 *
 * 字符串特点：
 * 1、内容不可变
 * 2、可共享使用
 * 3、效果上相当于char[]字符数组，底层原理是byte[]字节数组
 *
 * 三种构造方法
 * 1、String()
 * 2、String(char[] array)
 * 3、String(byte[] array)
 *
 * 直接创建
 * String str = "Hello ";
 * 字符串常量池：程序当中直接写上的双引号字符串，就在字符串常量池中，new不在池中
 *
 * 对于基本类型：==是进行数值比较
 * 对于引用类型：==是进行地址值比较
 */
public class CString {
    public static void main(String[] args) {
        String str1 = new String();
        System.out.println("第一个字符串："+str1);

        char[] charArray = {'A', 'B', 'C'};
        String str2 = new String(charArray);
        System.out.println("第二个字符串："+str2);

        byte[] byteArray = {97, 98, 99};
        String str3 = new String(byteArray);
        System.out.println("第三个字符串："+str3);

        // 字符串比较内容
        System.out.println(str3.equals(str2));
        System.out.println(str3.equalsIgnoreCase(str2));

        // 获取字符个数
        System.out.println(str3.length());

        // 拼接字符串
        String str4 = str3.concat("DEF");
        System.out.println(str3);
        System.out.println(str4);

        // 索引位置字符
        System.out.println(str3.charAt(1));

        // 查找子字符串首次出现的位置
        System.out.println(str3.indexOf("bc"));

        // String substring(int index) 截取index到末尾
        // String substring(int begin, int end) 截取[begin, end)
        String str5 = str4.substring(2);
        System.out.println(str5);

        // 转换为字符数组
        char[] chars = str5.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            System.out.println(chars[i]);
        }

        // 转换为字符数组
        byte[] bytes = str5.getBytes();
        for (int i = 0; i < bytes.length; i++) {
            System.out.println(bytes[i]);
        }

        // String replace(charSequence oldString, CharSequence newString)
        String str6 = "cao ni ma";
        String str7 = str6.replace("ma", "family");
        System.out.println(str7);

        // String[] split(String regex)将字符串切割成若干部分
        // 注：参数是正则表达式，用"."时需"\\."
        String[] arra = str6.split(" ");
        for (int i = 0; i < arra.length; i++) {
            System.out.println(arra[i]);
        }
    }
}
