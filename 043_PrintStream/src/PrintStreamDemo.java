import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * PrintStream 为其他输出流添加功能，能方便打印各种数值表示形式
 * PrintStream(File file) 输出目的地是文件
 * PrintStream(OutputStream out) 输出目的地是字节输出流
 * PrintStream(String fileName) 输出目的地是文件路径
 * 1、只输出不读取
 * 2、不抛IOException
 * 3、print(任意类型值)，println(任意类型值并换行)
 */
public class PrintStreamDemo {
    public static void main(String[] args) {
        try{
//            test1();
            test2();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }

    public static void test1() throws FileNotFoundException {
        PrintStream ps = new PrintStream("test.txt");
        ps.write(97);
        ps.println(97);
        ps.println('A');
        ps.println("德玛西亚");
        ps.println(true);

        ps.close();
    }

    public static void test2() throws FileNotFoundException {
        PrintStream ps = new PrintStream("test.txt");
        System.setOut(ps);

        System.out.println("艾欧尼亚");
        ps.close();

    }
}
