import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * ServerSocket
 * ServerSocket(int port)
 * Socket accept()
 */
public class TCPServer {
    public static void main(String[] args) {
        try{
            test1();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void test1() throws IOException {

        int len = -1;
        byte[] bytes = new byte[1024];

        ServerSocket server = new ServerSocket(8080);
        Socket socket = server.accept();
        InputStream is = socket.getInputStream();
        OutputStream os = socket.getOutputStream();

        while(true){
            len = is.read(bytes);
            if(len > 0){
                System.out.println(new String(bytes,0,len));
                os.write("你也好啊".getBytes());
            }
        }

//        socket.close();
//        server.close();
    }
}
