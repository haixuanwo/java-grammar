import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Socket(String host, int port)
 * OutputStream getOutputStream() 获取输出流
 * InputStream getInputStream()   获取输入流
 * void close()
 */
public class TCPClient {
    public static void main(String[] args) {
        try{
            test1();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void test1() throws IOException {

        int len = -1;
        byte[] bytes = new byte[1024];

        Socket socket = new Socket("127.0.0.1", 8080);
        OutputStream os = socket.getOutputStream();
        InputStream is = socket.getInputStream();

        while (true){
            os.write("你好".getBytes());
            len = is.read(bytes);
            if(len > 0){
                System.out.println(new String(bytes,0,len));
            }
        }

//        socket.close();
    }
}
