
/**
 * java抽象只声明不实现
 * 抽象方法：使用abstract修饰，类型必须是抽象类
 * 特点：
 * 1、抽象类不可创建对象
 * 2、抽象子类必须重写父类中的方法，否则子类必须也是抽象类
 */
public abstract class Animal { // 类中若有抽象方法，必须是一个抽象类
    public abstract void eat();

    public abstract void dong();

    // 抽象类中可以有正常的方法
    public void smell(){
        System.out.println("我在smell");
    }
}
