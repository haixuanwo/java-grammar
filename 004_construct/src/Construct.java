public class Construct {
    /**
     * 1、静态构造器
     * 2、通用构造器
     * 3、构造方法->创建对象
     */

    static{
        System.out.println("这里是静态构造器");
    }

    {
        System.out.println("这里是通用构造器");
    }

    Construct(){
        System.out.println("这里是构造函数");
    }

    public static void main(String[] args){
        new Construct();
    }
}