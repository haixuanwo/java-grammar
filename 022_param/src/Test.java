/**
 * 值传递：把变量的值作为参数进行传递
 * 引用传递：直接把变量作为参数进行传递
 *
 * java使用的是值传递
 * */
public class Test {
//    public static void change(int n){
//        n = 20;
//    }

    public static void change(Cat c){
//        c = new Cat("火猫");
        c.name = "土猫";
    }

    public static void main(String[] args) {
        Cat c = new Cat("蓝猫");
        change(c);
        System.out.println(c.name);
    }
}
