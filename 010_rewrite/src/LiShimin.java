public class LiShimin extends LiYuan{

    // 重写也叫子类的覆盖
    public void makeCountry(){
        super.makeCountry(); // 半盖
        System.out.println("李世民也想建立一个自己的国家");
    }

    public static void main(String[] args){
        LiShimin lsm = new LiShimin();
        lsm.makeCountry();
    }
}
