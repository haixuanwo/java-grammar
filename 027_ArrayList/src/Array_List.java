import java.util.ArrayList;

/**
 * 长度可随意变化
 * 泛型：类型统一，泛型只能是引用类型，不能是基本类型
 * 注：打印为内容
 *
 * 常用方法：
 * 1、add(E e)添加元素
 * 2、get(int index)获取元素
 * 3、remove(int index)删除元素
 * 4、size()获取集合中元素个数
 *
 * 使用包装类存放基本数据类型
 * byte     Byte
 * short    Short
 * int      Integer [特殊]
 * long     Long
 * float    Float
 * double   Double
 * char     Character   [特殊]
 * boolean  Boolean
 */
public class Array_List {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("赵丽颖");
        list.add("谭慧芳");
//        System.out.println(list);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        list.add(0, "谭恒");
        System.out.println(list);

        list.remove(2);
        System.out.println(list);

        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(100);
        list1.add(200);
        list1.add(300);
        System.out.println(list1);
    }
}
