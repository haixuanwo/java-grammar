/**
 * 异常分类
 * 1、RuntimeException：运行时异常，一般不手动处理，出问题了再处理
 * 2、其他Exception：必须经过手动处理
 * 3、Error：一般指的是系统级错误
 */
public class Test {
    public static void main(String[] args) {
        try {
            System.out.println(1 / 0); // 0不能做除数
        } catch (Exception e){
            //e.printStackTrace(); // 打印错误信息
            System.out.println("系统出错了，请联系管理员"); // 给客户看
        } finally { // 一般做收尾工作
            System.out.println("你好啊，我是finally");
        }


        System.out.println("呵呵");

        // 1、异常时错误。运行时异常
        // 2、抛异常。创建错误对象，把错误对象丢出来
        // 3、捕获异常。默认由JVM来把错误信息进行捕获。打印出来。JVM会终止程序的执行

        // 编译时异常
        // 运行时异常
        // 抛异常
        // 捕获异常，默认是JVM来捕获
    }
}
