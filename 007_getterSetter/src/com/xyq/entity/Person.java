package com.xyq.entity;

public class Person {
    String name;
    int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0)
            this.age = 0;
        else
            this.age = age;
    }
//    public void setName(String name){
//        this.name = name;
//    }
//
//    public void setAge(int age){
//        if (age < 0)
//            this.age = 0;
//        else
//            this.age = age;
//    }
//
//    public String getName(){
//        return name;
//    }
//
//    public int getAge(){
//        return age;
//    }

    public void chi(){
        System.out.println(this.name+"在吃东西");
    }
}
