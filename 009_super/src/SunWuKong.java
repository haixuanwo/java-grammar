
/**
 * 创建对象过程：先创建父类对象，再创建子类对象
 * 1、super可以获取到父类中的内容
 * 2、可以调用父类中的构造方法，必须写在子类构造方法的第一行，如果父类的构造是无参的，可以不写。
 *    如果父类没有无参构造，必须要写super
 */
public class SunWuKong extends Hero{
    String name = "孙大圣";

    public SunWuKong(){
        // super()  隐含着调用父类的构造，在子类调用的第一行默认调用父类的方法
        super("武大郎");
        System.out.println("我是子类的构造方法");
    }

    public void chi(){
        // 想看到父类中变量，super区分父类与子类重名内容
        System.out.println(super.name+"在吃桃子");

        // 先找自己类，再找父类
        System.out.println(this.name+"在吃桃子");
    }

    public static void main(String[] args){
        SunWuKong s = new SunWuKong();
        s.chi();
    }
}
