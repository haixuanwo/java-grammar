
/**
 * 能继承接口的只能是接口
 * 接口和类只能是实现关系 implement
 * */
public interface Valuable {
    // 接口中所有的方法都是抽象方法，可以省略掉abstract
    // 接口中所有的内容都是公开，公共的

    // public static final int money = 100; // 默认形式
    int money = 100;

    void getMoney(); // 接口中所有的方法都是抽象方法，不可以有正常方法
}
