/**
 * 接口实际上是一种特殊的抽象类
 * 接口中所有的方法都是抽象方法
 * 接口使用interface来声明
 *
 * 类只能单继承，接口支持多实现
 * 接口同样具有多态性
 * 接口可以把很多不相关的内容进行整合
 *
 * 特点：
 * 1、接口中所有的方法都是抽象方法，都是公开的
 * 2、接口中所有的变量都是全局静态常量
 */
public class Test {
    public static void main(String[] args){
//        Valuable g = new Gold(); // 接口一样具有多态性
//        g.getMoney();
        Panda p = new Panda();
        Animal ani = new Panda();
        Valuable v = new Panda();
        Protectable pro = new Panda();

//        p.eat();
//        p.cang();
//        p.getMoney();

//        ani.eat();
//        ani.cang();
//        ani.getMoney();
        v.getMoney();
        pro.cang();

        System.out.println(Valuable.money);
    }
}
