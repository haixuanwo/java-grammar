/**
 * final 不可改变的
 * 1、修饰的变量为常量
 * 2、修饰的方法不可被重写
 * 3、修饰的类不可被继承
 */

public class Diamands {
    final int weight = 10;

    public final void bling(){
        System.out.println("布灵布灵的。");
    }

    public static void main(String[] args){
        Diamands d = new Diamands();
//        d.weight = 100;

    }
}
