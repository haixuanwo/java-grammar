import java.io.*;

/**
 * 一、FileOutputStream
 * FileOutputStream(String name)
 * FileOutputStream(File file)
 * FileOutputStream(String name, boolean append) 以追加的方式创建
 * FileOutputStream(File file, boolean append)
 *
 * 写数据（内存-->硬盘）
 *      java程序->JVM->OS（操作系统）->硬盘文件
 *
 * 方法：
 *  1、void write(byte[] b)    全部写入
 *  2、void write(byte[] b, int off, int len)  指定起始位与长度写入
 *  3、int read()            读取一个字节
 *  4、int read(byte[] b)    读取一定数量字节
 *
 * 二、FileInputStream
 * FileOutputStream(String name)
 * FileOutputStream(File file)
 *
 * 三、FileReader(String name)
 *    FileReader(File file)
 *    FileReader(String name, boolean append)
 *    FileReader(File file, boolean append)
 * 1个中文
 *         GBK:占用两个字节
 *         UTF-8:占用3个字节
 *
 *  四、FileWrite(String)
 *     FileWrite(File file)
 *
 *     - void write(int c) 写入单个字符。
 *     - void write(char[] cbuf)写入字符数组。
 *     - abstract  void write(char[] cbuf, int off, int len)写入字符数组的某一部分,off数组的开始索引,len写的字符个数。
 *     - void write(String str)写入字符串。
 *     - void write(String str, int off, int len) 写入字符串的某一部分,off字符串的开始索引,len写的字符个数。
 *     - void flush() 刷新该流的缓冲。
 *     - void close() 关闭此流，但要先刷新它。
 */
public class FileOutputStreamDemo {
    public static void main(String[] args){
        try {
//            test1Write();
//            test2Read();
//            test3CopyFile();
//            test4FileReader();
            test5FileWrite();
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    public static void test5FileWrite() throws IOException {
        FileWriter fw = new FileWriter("IO.txt");
        fw.write("风遁螺旋丸手里剑");
        fw.flush();
        fw.close();

        fw = new FileWriter("IO.txt", true);
        fw.write("地爆天星");
        fw.flush();
        fw.close();
    }

    public static void test4FileReader() throws IOException {
        FileReader fr = new FileReader("IO.txt");

        int len = 0;
        char[] cs = new char[1024];
        while(true){
            len = fr.read(cs);
            if(-1 == len){
                break;
            }
            System.out.println(new String(cs,0,len));
        }
    }

    public static void test3CopyFile() throws IOException {
        FileInputStream fis = new FileInputStream("IO.txt");
        FileOutputStream fos = new FileOutputStream("IO_copy.txt");

        byte[] bytes = new byte[1024];
        int len = 0;

        while(true){
            len = fis.read(bytes);
            if(-1 == len) {
                break;
            }
            fos.write(bytes, 0, len);
        }

        fis.close();
        fos.close();
        System.out.println("现在时间："+System.currentTimeMillis());
    }

    public static void test2Read() throws IOException{
        FileInputStream fis = new FileInputStream("IO.txt");
        System.out.println(fis.read());

        byte[] bytes = new byte[1024];
        int len = fis.read(bytes);
        System.out.println("len="+len);

        for (int i = 0; i < len; i++) {
            System.out.println((char)bytes[i]);
        }
    }

    public static void test1Write() throws IOException{
        FileOutputStream fos = new FileOutputStream("IO.txt");

        fos.write(100);

        byte[] bytes = {65, 66, 67};
        fos.write(bytes);

        bytes = "haixuanwo".getBytes();
        fos.write(bytes);

        bytes = "漩涡鸣人".getBytes();
        fos.write(bytes);

        fos.close();

        fos = new FileOutputStream("IO.txt", true);
        bytes = "宇智波·佐助".getBytes();
        fos.write(bytes);

        fos.close();
    }
}
