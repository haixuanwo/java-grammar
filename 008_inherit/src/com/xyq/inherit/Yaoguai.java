package com.xyq.inherit;
/**
 * 继承：子类可以自动拥有父类中除了私有内容外的所有内容。 is-a关系
 * 作用：简化代码开发
 * **/
public class Yaoguai {
    String name;

    private void pasi(){
        System.out.println("妖怪怕死");
    }

    public void chiRen(){
        System.out.println("妖怪会吃人");
    }
}
