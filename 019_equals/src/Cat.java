public class Cat {
    String name;
    String color;

    public Cat(String name, String color){
        this.name = name;
        this.color = color;
    }

    // 重写object中的equals
    public boolean equals(Cat c){
        if (this.color == c.color){
            return true;
        }
        return false;
    }

    public static void main(String[] args){
        Cat c1 = new Cat("小花","红色");
        Cat c2 = new Cat("小花","红色");

        System.out.println(c1 == c2); // 默认判断两个对象的内存地址是否一致,一般用在基本数据类型
        System.out.println(c1.equals(c2));
    }
}
