package com.xyq.dao;

import com.xyq.entity.Person;

/**
 * 包：其实本质上就是文件夹
 * 在代码中需要packege包名
 * 导包：import 包+类
 *
 * 不需要导包：1、在自己包里。2、java.lang包下所有的内容
 * */

public class TestPerson {
    public static void main(String[] args){
        Person p = new Person();
    }
}
