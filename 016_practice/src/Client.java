import java.util.Scanner;

public class Client {
    public static void main(String[] args){
        System.out.println("请问你要链接的数据库是哪一个（1.MySQL. 2.Oracle）");

        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();

        IDAO dao;
        if (1 == n)
            dao = new MySqlDao();
        else
            dao = new OracleDao();

        dao.connect();
        dao.add();
        dao.del();
        dao.upd();
        dao.sel();
    }
}
