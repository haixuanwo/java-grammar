import java.io.*;

/**
 * 一、BufferedOutputStream 字节缓冲输出流
 * BufferedOutputStream(OutputStream out)
 * BufferedOutputStream(OutputStream out, int size)
 *
 * - public void close() ：关闭此输出流并释放与此流相关联的任何系统资源。
 * - public void flush() ：刷新此输出流并强制任何缓冲的输出字节被写出。
 * - public void write(byte[] b)：将 b.length字节从指定的字节数组写入此输出流。
 * - public void write(byte[] b, int off, int len) ：从指定的字节数组写入 len字节，从偏移量 off开始输出到此输出流。
 * - public abstract void write(int b) ：将指定的字节输出流。
 *
 * 二、BufferedInputStream 字节缓冲输入流
 * BufferedWriter(Writer out)
 * BufferedWriter(Writer out, int sz)
 * int read()
 * int read(byte[] b)
 * void close()
 *
 * 三、BufferedWriter 字符缓冲输出
 * - void write(int c) 写入单个字符。
 * - void write(char[] cbuf)写入字符数组。
 * - abstract  void write(char[] cbuf, int off, int len)写入字符数组的某一部分,off数组的开始索引,len写的字符个数。
 * - void write(String str)写入字符串。
 * - void write(String str, int off, int len) 写入字符串的某一部分,off字符串的开始索引,len写的字符个数。
 * - void flush()刷新该流的缓冲。
 * - void close() 关闭此流，但要先刷新它。
 *
 * 四、BufferedReader 字符缓冲读取
 * BufferedReader(Reader in)
 * BufferedReader(Reader in, int sz)
 */
public class BufferedStreamDemo {
    public static void main(String[] args) {
        try {
//            test1();
            test2();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void test1() throws IOException {
        FileOutputStream fos = new FileOutputStream("a.txt");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        bos.write("降龙十八掌".getBytes());
        bos.flush();
        bos.close();

        FileInputStream fis = new FileInputStream("a.txt");
        BufferedInputStream bis = new BufferedInputStream(fis);
        byte[] bytes = new byte[1024];
        int len = 0;
        while (true) {
            len = bis.read(bytes);
            if (-1 == len) {
                System.out.println(new String(bytes, 0, len));
            }
        }
//        bis.close();
    }

    public static void test2() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("a.txt"));
        for (int i = 0; i < 10; i++) {
            bw.write("海漩涡");
            bw.newLine();
        }
        bw.flush();
        bw.close();

        BufferedReader br = new BufferedReader(new FileReader("a.txt"));
        String line;
        while(true){
            line = br.readLine();
            if(null == line){
                break;
            }
            System.out.println(line);
        }
        br.close();
    }
}
