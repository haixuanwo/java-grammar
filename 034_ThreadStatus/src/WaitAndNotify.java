/**
 * wait()   一直等待
 * wait(long m) 等待毫秒
 * notify() 通知最先阻塞的线程
 * notifyAll()  通知所有线程
 */
public class WaitAndNotify {
    public static void main(String[] args) {
        // 创建锁对象，保证唯一
        Object obj = new Object();

        // 创建一个消费者
        new Thread(){
            @Override
            public void run() {
                while(true){
                    synchronized (obj) {
                        System.out.println("告知----->");
                        try {
                            obj.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println("ok-------->");
                        System.out.println("=====================");
                    }
                }
            }
        }.start();

        // 创建生产者
        new Thread() {
            @Override
            public void run() {
                while(true) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    synchronized (obj) {
                        System.out.println("通知--->ok");
                        obj.notify();
                    }
                }
            }
        }.start();
    }
}
