
/**
 * 多态：同一个对象拥有多种形态
 * 作用：把不同的数据类型进行统一，让程序具有超强的可扩展性
 * 小知识点：
 *  1、把子类的对象赋值给父类的变量->向上转型
 *  2、把父类变量转化回子类的变量->向下转型
 *      向下转型有可能有风险.java要求必须要写强制类型转换
 *      （转换之后的数据类型）变量
 *
 */

public class Client {
    public static void main(String[] args){
//        Cat c = new Cat();
//        Dog d = new Dog();
//
//        Person p = new Person();
//        p.feedCat(c);
//        p.feedDog(d);
//        Cat c = new Cat();
        // 可以把猫当成动物来看，把子类对象赋值给父类的引用变量向上转型
        // 会屏蔽掉子类中特有的方法
        Animal ani1 = new Cat();
        Animal ani2 = new Dog();
        Animal ani3 = new DaXiang();

//        Person p = new Person();
//        p.feed(ani1);
//        p.feed(ani2);
//        p.feed(ani3);
        Cat cc = (Cat)ani1;
        cc.catchMouse();
    }
}
