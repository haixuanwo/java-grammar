public class car {
    String color;
    int speed;
    int seat;
    String pos;

    public car(String color, int speed, int seat){
        this.color = color;
        this.speed = speed;
        this.seat = seat;
    }

    public car(String color, int speed, int seat, String pos){
        this(color, speed, seat);
        this.pos = pos;
    }

    public void run(){
        System.out.println("车能跑:"+pos);
    }

    public static void main(String[] args){
        car c = new car("红色",100,200, "深圳");
        c.run();
    }
}
