/**
 * 同步方法
 * 1、抽取共享数据到方法中
 * 2、在方法上加synchronized修饰符
 */
public class SyncMethod implements Runnable{
    private int ticket = 100;

    // 创建锁对象
    Object obj = new Object();

    @Override
    public void run() {
        while(true){
            sellTicket();
        }
    }

    public synchronized void sellTicket(){
        if(ticket > 0){
            try{
                Thread.sleep(10);
            }catch (InterruptedException e){
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName()+"--->正在卖"+ticket+"票");
            ticket--;
        }
    }
}
