import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 锁机制
 * 1、创建ReentranLock
 * 2、调用lock
 * 3、调用unlock释放
 */
public class SyncLock implements Runnable{
    private int ticket = 100;

    // 创建ReentrantLock对象
    Lock l = new ReentrantLock();

    @Override
    public void run() {
        while(true){
            l.lock();
            if(ticket > 0){
                System.out.println(Thread.currentThread().getName()+"--->正在卖"+ticket+"票");
                ticket--;

                try{
                    Thread.sleep(10);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }finally {
                    l.unlock();
                }
            }
        }
    }
}
