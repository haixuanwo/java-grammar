/**
 * 三种同步方法
 * 1、同步代码块
 * 2、同步方法
 * 3、锁机制
 */
public class ThreadSync {
    public static void main(String[] args) {
        // test1();
        // test2();
        test3();
    }

    public static void test1() {
        SyncCodeBlock ri = new SyncCodeBlock();
        new Thread(ri).start();
        new Thread(ri).start();
        new Thread(ri).start();
    }

    public static void test2() {
        SyncMethod sm = new SyncMethod();
        new Thread(sm).start();
        new Thread(sm).start();
        new Thread(sm).start();
    }

    public static void test3() {
        SyncLock sl = new SyncLock();
        new Thread(sl).start();
        new Thread(sl).start();
        new Thread(sl).start();
    }
}
