/**
 * 数组的概念：是一种容器，可以同时存放多个数据值
 * 数组特点：
 * 1、数组是一种引用数据类型
 * 2、数组当中的多个数据，类型必须统一
 * 3、数组的长度在程序运行期间不可改变
 *
 * 数组初始化
 * 1、动态，指定长度
 * 2、静态：指定内容
 */
public class Array {
    public static void main(String[] args) {
        int data[] = new int[10];
        data[0] = 100;
        for (int i:data) {
            System.out.println(i);
        }

        int value[] = {1, 2, 3, 4, 5};
        for (int i = 0; i < value.length; i++) {
            System.out.println(value[i]);
        }
    }
}
