/**
 * (parameters)->expression
 * (parameters)->{ statements; }
 *
 * 可选类型声明：不需要声明参数类型，编译器可以统一识别参数值
 * 可选参数圆括号：一个参数无需定义圆括号
 * 可选大括号：一个语句不需大括号
 * 可选返回关键字：只一个表达式返回值，编译器自动返回
 *
 * 1.不需要参数，返回值5
 *  () -> 5
 *
 * 2.接收一个参数（数字类型），返回其2倍的值
 *  x -> 2*x
 *
 * 3.接受2个参数（数字），并返回他们的差值
 *  (x, y) -> x-y
 *
 * 4.接收2个int型整数，返回他们的和
 *  (int x, int y) -> x + y
 *
 * 5.接受一个string对象，并在控制台打印，不返回
 *  (String s) -> System.out.println(s);
 *
 *  Lambda使用前提
 *  1、必须具有接口，且仅有一个抽象方法
 *  2、必须具有上下文推断
 */
public class CLambda {
    public static void main(String[] args) {
        //test1();
        test2();
    }

    public  static void test1(){
        //        RunnableImpl run = new RunnableImpl();
//        Thread t  = new Thread(run);
//        t.start();
//
//        // 简化代码
//        Runnable r = new Runnable(){
//            @Override
//            public void run() {
//                System.out.println(Thread.currentThread().getName()+"新线程创建了");
//            }
//        }
//        new Thread(r).start();
//
//        // 简化代码
//        new Thread(new Runnable(){
//            @Override
//            public void run() {
//                System.out.println(Thread.currentThread().getName()+"新线程创建了");
//            }).start();

        // 使用匿名内部类的方式，实现多线程
        new Thread(new Runnable(){
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+"匿名：新线程创建了");
            }
        }).start();

        // 使用lambda表达式，实现多线程，重写了run
        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"lambda：新线程创建了");
        }).start();
    }

    public  static void test2(){
        // lambda函数直接重写接口里的函数
        InterfaceLambda il = ()->{
            System.out.println("德玛西亚");
        };

        il.tanhen();
    }
}
