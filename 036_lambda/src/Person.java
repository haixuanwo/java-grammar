import java.util.Arrays;

public class Person {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public static void main(String[] args) {
        Person[] arr = {
                new Person("柳岩", 238),
                new Person("奥特曼", 138),
                new Person("超人", 1138),
        };

        Arrays.sort(arr, (Person p1, Person p2)->{return p1.age - p2.age;});

        for (Person p : arr) {
            System.out.println(p.name+p.age);
        }
    }
}
