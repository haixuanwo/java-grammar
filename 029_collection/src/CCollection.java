/**
 * 集合是一种容器，可用来存储多个数据。
 * 长度是可变的，集合存储的都是对象，且类型可以不一致
 *
 * 1、会使用集合存储数据
 * 2、会增删查改
 * 3、特性
 *
 * Collection集合常用方法
 * boolean add(E e) 添加元素
 * boolean remove(E e) 删除元素
 * boolean isEmpty() 是否为空
 * void clear() 清空集合
 * boolean contains(E e) 判断是否包含某元素
 * int size()   集合长度
 * Object[] toArray()   将集合转成数组
 *
 * List接口
 * 1、有序
 * 2、重复元素
 * 3、有索引
 *
 * Vectoe
 * ArrayList：底层是数组实现，查询快、增删慢
 * LinkedList：底层是链表实现，查询慢、增删快
 *
 * Set接口
 * 1、元素唯一
 * 2、无索引
 * 3、无序
 *
 * HashSet：底层是哈希表+(红黑树)实现的
 * LinkedHashSet：底层哈希表+链表实现
 * TreeSet：底层是二叉树实现，一般用于排序
 */
public class CCollection {
}
