import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;

/**
 * boolean add(E e) 添加元素
 * boolean remove(E e) 删除元素
 * boolean isEmpty() 是否为空
 * void clear() 清空集合
 * boolean contains(E e) 判断是否包含某元素
 * int size()   集合长度
 * Object[] toArray()   将集合转成数组
 */
public class CArrayList {
    public static void main(String[] args) {
        Collection<String> coll = new ArrayList<>();
        coll.add("nima");
        coll.add("张无忌");
        coll.add("刘德华");
        System.out.println(coll);
        System.out.println(coll.size());

        coll.remove("刘德华");
        System.out.println(coll);

        Object[] arr = coll.toArray();
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        System.out.println(coll.contains("谭孝海"));
        coll.clear();
        System.out.println(coll.isEmpty());
   }
}
