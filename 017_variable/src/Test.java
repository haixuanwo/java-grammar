
/**
 * 基本数据类型默认值都是0，包括boolean->false
 * 引用数据类型：null
 * null表示空，什么都没有，占位
 */

public class Test {
    public static void main(String[] args){
        int n = 100; // 所有变量必须先声明后赋值才能使用

        Person p = new Person();
        System.out.println("byte="+p.b);
        System.out.println("short="+p.s);
        System.out.println("int="+p.i);
        System.out.println("long="+p.l);
        System.out.println("float="+p.f);
        System.out.println("double="+p.d);
        System.out.println("boolean="+p.bool);
        System.out.println("char="+p.c);
        System.out.println("str="+p.str);
        System.out.println("child="+p.child);
    }
}
