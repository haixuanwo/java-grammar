public class Person {
//    int age; // 成员变量在创建对象时会初始化为0
    byte b;
    short s;
    int i;
    long l;
    float f;
    double d;
    char c;
    boolean bool;

    String str; // 是java提供的一个类
    Child child; // 自己写的类
}
