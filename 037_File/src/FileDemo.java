import java.io.File;
import java.io.IOException;

/**
 *  file 文件
 *  directory 文件夹/目录
 *  path 路径
 *
 *  注：
 *  1、路径不分大小写
 *  2、windows反斜杠是转义字符，两个反斜杠代表一个普通反斜杠
 *
 *  File功能
 *  1、String getAbsolutePath()  获取绝的路径
 *  2、String getPath()  获取路径
 *  3、String getName()  获取名字
 *  4、long length()     文件长度
 *  5、boolean exists()  是否存在
 *  6、boolean isDirectory() 是否为目录
 *  7、boolean isFile()  是否为文件
 *  8、boolean createNewFile()   创建新文件
 *  9、boolean delete()  删除文件或目录
 *  10、boolean mkdir() 创建单级目录
 *  11、Boolean mkdirs() 创建多级目录
 *  12、String[] list()  返回所有子文件与目录，存String
 *  13、File[] listFiles()  返回所有子文件与目录，存File
 */
public class FileDemo {
    public static void main(String[] args) {
        System.out.println(File.pathSeparator); // 路径分隔符 ";"
        System.out.println(File.separator); // 文件名分隔符 linux"/" windows"\"

//        test1();
//        test2();

//        try {
//            test3();
//        }catch (IOException e){
//            e.printStackTrace();
//        }

        test4();
    }

    public static void test1(){
        // 当前路径文件
        File f1 = new File("a.txt");
        System.out.println(f1); // 输出pathname

        // 路径+文件名
        File f2 = new File("D:\\JAVA\\myProject\\037_File", "b.txt");
        System.out.println(f2); // 输出pathname

        // 父路径+子路径
        File parent = new File("D:\\JAVA\\myProject\\037_File");
        File file = new File(parent, "c.txt");
        System.out.println(file); // 输出pathname
    }

    public static void test2(){
        File f1 = new File("D:\\JAVA\\myProject\\037_File\\d.txt");
        System.out.println(f1.getAbsolutePath());
        System.out.println(f1.getPath());
        System.out.println(f1.getName());
        System.out.println(f1.length());

        File f2 = new File("e.txt");
        System.out.println(f2.getAbsolutePath());
        System.out.println(f2.getPath());
        System.out.println(f2.getName());
        System.out.println(f2.length());

        System.out.println(f1.exists());
        System.out.println(f1.isDirectory());
        System.out.println(f1.isFile());
    }

    public static void test3() throws IOException {
        File f1 = new File("D:\\JAVA\\myProject\\037_File\\f.txt");
        f1.createNewFile();
        System.out.println(f1.getAbsolutePath()+"是否存在："+f1.exists());
        f1.delete();
        System.out.println(f1.getAbsolutePath()+"是否存在："+f1.exists());

        File f2 = new File("D:\\JAVA\\myProject\\037_File\\thf\\txh");
        System.out.println("mkdir:"+f2.mkdir());
        System.out.println("mkdirs:"+f2.mkdirs());
    }

    public static void test4() {
        File f1 = new File("D:\\JAVA\\myProject\\037_File");
        String[] arrStr = f1.list();
        for (String s : arrStr) {
            System.out.println(s);
        }

        File[] files = f1.listFiles();
        for (File file : files) {
            System.out.println(file.getAbsolutePath());
        }
    }
}
