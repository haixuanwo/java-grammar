package com.getObject;

import com.domain.Person;

/**
 * 获取class对象的方式
 * 1、class.forName("全类名")：将字节码文件加载进内存，返回class对象
 * 2、类名.class:通过类名的属性class获取
 * 3、对象.getClass()方法在Object类中定义着
 */
public class GetClassObject {
    public static void main(String[] args) throws Exception{
        // 1.class.forName("全类名")
        Class cls1 = Class.forName("com.domain.Person");
        System.out.println(cls1);

        // 2.类型.class
        Class cls2 = Person.class;
        System.out.println(cls2);

        // 3.对象.getClass
        Person p = new Person();
        Class cls3 = p.getClass();
        System.out.println(cls3);

        // ==比较对象
        System.out.println(cls1==cls2);
        System.out.println(cls2==cls3);
        System.out.println(cls1==cls3);
    }
}
