/**
 * 匿名内部类创建线程
 * 匿名：无名字
 * 内部类：写在其他类的内部类
 *
 * 匿名内部类作用：简化代码
 *  1、把子类继承父类，重写父类方法，创建子类对象合一步完成
 *  2、把实现类实现类接口，重写接口中的方法，创建实现类对象和成一步完成
 *
 * 匿名内部类的最终产物：子类/实现类对象，而这个类没有名字
 *
 * 格式：
 *  new 父类/接口(){
 *      重复父类/接口中的方法
 *  };
 *
 */
public class CreateThread3 {
    public static void main(String[] args) {
        // 1、线程的父类是Thread
        new Thread(){
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName()+"石头人");
                }
            }
        }.start();

        // 2、线程接口runnable
        new Thread(new Runnable(){
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName()+"卡牌");
                }
            }
        }).start();
    }
}
