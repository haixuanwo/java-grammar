/**
 * 实现步骤：
 * 1、创建一个Thread类的子类
 * 2、子类中重写run
 * 3、子类对象调用start方法，来开启新的线程执行run方法
 */
public class CreateThread1 extends Thread{
    private String name;

    public CreateThread1(String name) {
        super(name);
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println(getName());

        // 链式编程
        System.out.println(Thread.currentThread().getName());

        for (int i = 0; i <2000 ; i++) {
            System.out.println(name+"-->"+i);
            try {
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
