/**
 * 实现步骤
 * 1、创建Runnable接口实现类
 * 2、在实现类中重写run
 * 3、创建Thread类对象，构造方法传入实现类对象
 * 4、Thread对象调用start
 */
public class CreateThread2 implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println(i);
            try {
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
