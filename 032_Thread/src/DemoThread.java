/**
 * 主线程：main方法的线程
 * 单线程程序：java程序中只有一个线程
 * 从main开始，依次执行
 *
 * 线程名称（默认）：
 *  主线程：main
 *  新线程：Thread-0,Thread-1...
 *
 * 设置线程名称：
 * 1、setName(String name)
 * 2、使用Thread带参构造方法
 *
 * 实现Runnable接口创建多线程好处：
 * 1、避免单继承的局限性
 * 2、增加了程序的扩展性，降低了耦合性
 */
public class DemoThread {
    public static void main(String[] args) {
        //test1();
        test2();
    }

    // 第一种创建线程的方式
    private static void test1() {
        CreateThread1 p1 = new CreateThread1("小钱");
//        p1.setName("小钱");
        p1.start();

        CreateThread1 p2 = new CreateThread1("寒冰");
//        p2.setName("寒冰");
        p2.start();
    }

    // 第二种创建线程的方式
    private static void test2() {
        CreateThread2 p1 = new CreateThread2();
        new Thread(p1).start();

        CreateThread2 p2 = new CreateThread2();
        new Thread(p2).start();
    }
}
