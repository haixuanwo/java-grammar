import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * java.util.conccurrent.Excutors:线程池工程类
 * java.util.conccurrent.ExcutorService:线程池接口
 *  用来从线程池中获取线程，调用start方法执行线程任务
 *      submit(Runnable task)
 *      void shutdown()
 */
public class ThreadPool {
    public static void main(String[] args) {
        // 1、使用newFixedThreadPool生产线程池
        ExecutorService es = Executors.newFixedThreadPool(2);

        // 2、submit添加任务
        es.submit(new RunnableImpl());
        es.submit(new RunnableImpl());

        // 3、shutdown销毁线程池
//        es.shutdown();
    }
}
